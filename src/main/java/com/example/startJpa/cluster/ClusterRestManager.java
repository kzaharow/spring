package com.example.startJpa.cluster;

import com.example.startJpa.dto.UserDto;
import com.example.startJpa.service.ChangeLogRootServiceImpl;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class ClusterRestManager implements Manager<UserDto> {

    private final static int MASTER_NODE = 0;

    private final static String DEFAULT_PORT = "10001";

    @Autowired
    private ChangeLogRootServiceImpl changeLogService;

    @Autowired
    private Cluster cluster;


    @Override
    public boolean isRedirect(String passportId) {

        String currentClusterName = cluster.getName();
        String prefix = passportId.substring(0, 2);
        String targetCluster = cluster.getPrefix().get(prefix);
        return !currentClusterName.equals(targetCluster);
    }

    @Override
    public ResponseEntity<?> redirectGet(ResponseEntity<?> responseEntity, String passportId) {

        String port = getTargetPortByPassportPrefix(passportId);
        try {
            String url = String.format(cluster.getUrlUserPassport(), port, passportId);
            responseEntity = new RestTemplate().getForEntity(url, String.class);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<?> redirectPost(ResponseEntity<?> responseEntity, UserDto userDto) {

        String passportId = userDto.getPassport();
        if (isRedirect(passportId)) {
            String port = getTargetPortByPassportPrefix(passportId);
            try {
                String url = String.format(cluster.getUrlUser(), port);
                responseEntity = new RestTemplate().postForEntity(url, userDto, String.class);
            } catch (Exception e) {
                log.error(e.getMessage());
            }
        }
        return responseEntity;
    }

    @Override
    public ResponseEntity<?> redirectDelete(ResponseEntity<?> responseEntity, String passportId) {

        String port = getTargetPortByPassportPrefix(passportId);
        try {
            String url = String.format(cluster.getUrlUserPassport(), port, passportId);
            responseEntity = new RestTemplate().exchange(url, HttpMethod.DELETE, null, String.class);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return responseEntity;
    }

    private String getTargetPortByPassportPrefix(String passportId) {

        String prefix = passportId.substring(0, 2);
        String targetCluster = cluster.getPrefix().get(prefix);
        String targetUrl = cluster.getInfo().get(targetCluster).get(MASTER_NODE);
        return (targetUrl != null) ? targetUrl : DEFAULT_PORT;
    }


}
