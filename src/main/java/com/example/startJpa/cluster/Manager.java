package com.example.startJpa.cluster;

import org.springframework.http.ResponseEntity;

public interface Manager<T> {

    boolean isRedirect(String passportId);

    ResponseEntity<?> redirectGet(ResponseEntity<?> responseEntity, String passportId);

    ResponseEntity<?> redirectPost(ResponseEntity<?> responseEntity, T t);

    ResponseEntity<?> redirectDelete(ResponseEntity<?> responseEntity, String passportId);
}
