package com.example.startJpa.cluster;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

@Data
@Configuration
@ConfigurationProperties(prefix = "fields")
public class Cluster {

    private Map<String, String> prefix;
    private String name;
    private List<String> ports;
    private ClusterStatus status;
    private Map<String, List<String>> info;
    private String urlHealth;
    private String urlLog;
    private String urlUser;
    private String urlUserPassport;
}
