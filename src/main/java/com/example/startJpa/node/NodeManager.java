package com.example.startJpa.node;

import com.example.startJpa.cluster.Cluster;
import com.example.startJpa.cluster.ClusterStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

@Component
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
public class NodeManager {

    private final Node node;

    private final Cluster cluster;

    @Scheduled(fixedDelay = 5000, initialDelay = 1000)
    public void scheduleFixedDelayTask() {

        node.setNodeStatus(NodeStatus.SLAVE);
        int counter = 0;
        ArrayList<String> ports = (ArrayList) cluster.getPorts();
        for (String port : ports) {
            try {
                String url = String.format(cluster.getUrlHealth(), port);
                ResponseEntity<String> responseEntity = new RestTemplate().getForEntity(url, String.class);
                if (responseEntity.getStatusCode().value() == 200) {
                    if ((node.getPort().equals(port)) && (counter == 0)) {
                        node.setNodeStatus(NodeStatus.MASTER);
                    }
                    counter++;
                }
            } catch (Exception e) {

            }
        }
        if (counter > 1) {
            cluster.setStatus(ClusterStatus.GREEN);
        } else {
            cluster.setStatus(ClusterStatus.RED);
        }
        log.info(cluster.getName() + "->" + cluster.getStatus().name());
        log.info(node.getPort() + "->" + node.getNodeStatus().name());
    }
}
