package com.example.startJpa.node;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Scope("singleton")
@Component
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Node {

    private NodeStatus nodeStatus;
    private String port;
}


