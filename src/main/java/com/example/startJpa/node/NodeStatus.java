package com.example.startJpa.node;

public enum NodeStatus {
    MASTER,
    SLAVE
}
