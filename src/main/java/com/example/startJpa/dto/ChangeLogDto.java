package com.example.startJpa.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class ChangeLogDto {

    private Long id;
    private String type;
    private String data;
    private Date date;

}