package com.example.startJpa.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "changelog")
@Data
@Accessors(chain = true)
@AllArgsConstructor
@NoArgsConstructor

public class ChangeLog {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String type;


    @Column
    private String data;


    @Column
    @Temporal(TemporalType.TIMESTAMP)
    private Date date;

}
