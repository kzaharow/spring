package com.example.startJpa.utils;

import com.example.startJpa.dto.UserDto;
import com.example.startJpa.entity.Users;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.expression.ParseException;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class DtoToEntityConverter {

    private final ModelMapper modelMapper;

    /**
     * @param userDto an object for convert from dto to entity
     * @return converted entity
     * @throws ParseException when parsing error
     */
    public Users convertToEntity(UserDto userDto) throws ParseException {

        return modelMapper.map(userDto, Users.class);
    }


}
