package com.example.startJpa.utils;

import com.example.startJpa.entity.ChangeLog;
import com.example.startJpa.entity.Users;
import com.example.startJpa.service.ChangeLogRootServiceImpl;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Date;

@Component
@RequiredArgsConstructor
@Slf4j
public class ChangeLogSaver {

    private final ChangeLogRootServiceImpl changeLogService;

    /**
     * Add data / time to changelog. Uses for POST, DELETE, UPDATE
     *
     * @param type  - operation
     * @param users - entity
     */
    public void saveChangeLog(String type, Users users) {

        try {
            ChangeLog changeLog = new ChangeLog();
            changeLog.setType(type).
                    setData(new ObjectMapper().writeValueAsString(users)).
                    setDate(new Date());
            changeLogService.saveRecord(changeLog);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage());
        }
    }
}
