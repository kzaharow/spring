package com.example.startJpa.config;

import com.example.startJpa.cluster.Cluster;
import com.example.startJpa.node.Node;
import com.example.startJpa.node.NodeStatus;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.scheduling.annotation.EnableScheduling;

import javax.annotation.PostConstruct;

@Configuration
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
public class AppConfig {

    private final Node node;

    private final Environment environment;

    private final Cluster cluster;

    @Bean
    public ModelMapper modelMapper() {
        return new ModelMapper();
    }

    @PostConstruct
    void init() {

        node.setPort(environment.getActiveProfiles()[0]);
        node.setNodeStatus(NodeStatus.SLAVE);
        cluster.setPorts(cluster.getInfo().get(environment.getActiveProfiles()[1]));
    }
}
