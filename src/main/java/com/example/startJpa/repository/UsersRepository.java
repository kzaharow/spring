package com.example.startJpa.repository;

import com.example.startJpa.entity.Users;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UsersRepository extends JpaRepositoryImplementation<Users, Long> {

    Users findUsersByPassportEquals(String id);

    List<Users> deleteByPassportEquals(String id);

}
