package com.example.startJpa.repository;

import com.example.startJpa.entity.Streets;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

@Repository
public interface StreetsRepository extends JpaRepositoryImplementation<Streets, Long> {
}
