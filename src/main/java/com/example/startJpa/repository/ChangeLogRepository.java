package com.example.startJpa.repository;

import com.example.startJpa.entity.ChangeLog;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

import java.util.Date;
import java.util.List;

@Repository
public interface ChangeLogRepository extends JpaRepositoryImplementation<ChangeLog, Long> {

    /*@Query(value = "select c from changelog AS c"
            + " where c.date > :date", nativeQuery = true)
    List<ChangeLog> findRequestsForUpdate(@Param("date") String date);*/

    @Query(value = "SELECT max(date) FROM ChangeLog")
    Date findRecordWithMaxDate();


    List<ChangeLog> findAllByDateGreaterThan(Date date);

    boolean existsByDateEquals(Date date);
}
