package com.example.startJpa.repository;

import com.example.startJpa.entity.Cities;
import org.springframework.data.jpa.repository.support.JpaRepositoryImplementation;
import org.springframework.stereotype.Repository;

@Repository
public interface CitiesRepository extends JpaRepositoryImplementation<Cities, Long> {
}
