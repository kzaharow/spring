package com.example.startJpa.service;

import com.example.startJpa.entity.Users;
import com.example.startJpa.repository.UsersRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserRootServiceImpl implements RootService<Users> {

    private final UsersRepository usersRepository;

    public Users findUserByPassportId(String id) {

        return usersRepository.findUsersByPassportEquals(id);
    }

    @Transactional
    public List<Users> deleteUserByPassportId(String id) {

        List<Users> u = new ArrayList<>();
        try {
            u = usersRepository.deleteByPassportEquals(id);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return u;
    }

    @Override
    public List<Users> findAll() {

        return usersRepository.findAll();
    }

    @Override
    public Users saveRecord(Users users) {

        Users u = null;
        try {
            u = usersRepository.save(users);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return u;
    }

    @Override
    public Users deleteRecord(Long id) {

        Users users = getRecord(id);
        if (users != null) {
            usersRepository.delete(users);
        }
        return users;
    }

    @Override
    public void updateRecord(Users users) {

        try {
            Users updUser = usersRepository.findUsersByPassportEquals(users.getPassport());
            updUser.setName(users.getName());
            updUser.setEmail(users.getEmail());
            usersRepository.save(updUser);
        } catch (Exception e){
            log.error(e.getMessage());
        }
    }

    @Override
    public Users getRecord(Long id) {

        Optional<Users> optional = usersRepository.findById(id);
        return optional.orElse(null);
    }
}
