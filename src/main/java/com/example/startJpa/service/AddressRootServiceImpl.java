package com.example.startJpa.service;

import com.example.startJpa.entity.Address;
import com.example.startJpa.repository.AddressRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class AddressRootServiceImpl implements RootService<Address> {

    private final AddressRepository addressRepository;

    @Override
    public List<Address> findAll() {
        return null;
    }

    @Override
    public Address saveRecord(Address address) {
        return null;
    }

    @Override
    public Address deleteRecord(Long id) {
        return null;
    }

    @Override
    public void updateRecord(Address address) {

    }

    @Override
    public Address getRecord(Long id) {
        return null;
    }

}
