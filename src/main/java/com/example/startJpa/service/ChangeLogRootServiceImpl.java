package com.example.startJpa.service;

import com.example.startJpa.entity.ChangeLog;
import com.example.startJpa.repository.ChangeLogRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class ChangeLogRootServiceImpl implements RootService<ChangeLog> {

    private final ChangeLogRepository changeLogRepository;

    public boolean isExistRecord(Date date){
        return changeLogRepository.existsByDateEquals(date);
    }

    public Date findDateOfLastChangeLogRecord() {

        Date date = new Date();
        try {
            date = changeLogRepository.findRecordWithMaxDate();
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return date;
    }


    public List<ChangeLog> findChangeLogRecordsForUpdate(Date date) {

        List<ChangeLog> changeLogList = new ArrayList<>();
        try {
            changeLogList = changeLogRepository.findAllByDateGreaterThan(date);
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return changeLogList;
    }


    @Override
    public List<ChangeLog> findAll() {
        return null;
    }

    @Override
    public ChangeLog saveRecord(ChangeLog changeLog) {

        return changeLogRepository.save(changeLog);
    }

    @Override
    public ChangeLog deleteRecord(Long id) {

        return null;
    }

    @Override
    public void updateRecord(ChangeLog changeLog) {

    }

    @Override
    public ChangeLog getRecord(Long id) {

        return null;
    }
}
