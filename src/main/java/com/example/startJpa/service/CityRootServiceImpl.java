package com.example.startJpa.service;

import com.example.startJpa.entity.Cities;
import com.example.startJpa.repository.CitiesRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CityRootServiceImpl implements RootService<Cities> {

    private final CitiesRepository citiesRepository;

    @Override
    public List<Cities> findAll() {
        return null;
    }

    @Override
    public Cities saveRecord(Cities cities) {
        return null;
    }

    @Override
    public Cities deleteRecord(Long id) {
        return null;
    }

    @Override
    public void updateRecord(Cities cities) {

    }

    @Override
    public Cities getRecord(Long id) {
        return null;
    }

    public boolean ifExist(String name) {
        return true;
    }
}
