package com.example.startJpa.service;

import java.util.List;

public interface RootService<T> {

    List<T> findAll();

    T saveRecord(T t);

    T deleteRecord(Long id);

    void updateRecord(T t);

    T getRecord(Long id);


}
