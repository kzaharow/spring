package com.example.startJpa.changelog;

import com.example.startJpa.cluster.Cluster;
import com.example.startJpa.entity.ChangeLog;
import com.example.startJpa.entity.Users;
import com.example.startJpa.node.Node;
import com.example.startJpa.service.ChangeLogRootServiceImpl;
import com.example.startJpa.service.UserRootServiceImpl;
import com.google.gson.Gson;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.util.Date;
import java.util.List;

@Component
@RequiredArgsConstructor
@EnableScheduling
@Slf4j
public class ChangeLogManager {

    private final Node node;

    private final Cluster cluster;

    private final ChangeLogRootServiceImpl changeLogIService;

    private final UserRootServiceImpl userRootService;

    @Scheduled(fixedDelay = 5000, initialDelay = 2000)
    public void scheduleFixedDelayTask2() {

        Date dateOfLastChangeLogRecord = changeLogIService.findDateOfLastChangeLogRecord();
        for (String port : cluster.getPorts()) {
            if (!port.equals(node.getPort())) {
                try {
                    String url = String.format(cluster.getUrlLog(), port);
                    ResponseEntity<List<ChangeLog>> responseEntity = new RestTemplate().exchange(
                            url + dateOfLastChangeLogRecord,
                            HttpMethod.GET,
                            null,
                            new ParameterizedTypeReference<List<ChangeLog>>() {
                            });
                    List<ChangeLog> cl = responseEntity.getBody();
                    log.info(cl.toString());
                    for (ChangeLog record : cl) {
                        if (!changeLogIService.isExistRecord(record.getDate())) {
                            record.setId(null);
                            changeLogIService.saveRecord(record);
                            Users users = new Gson().fromJson(record.getData(), Users.class);
                            switch (record.getType()) {
                                case "POST":
                                    userRootService.saveRecord(users);
                                    break;
                                case "DELETE":
                                    userRootService.deleteUserByPassportId(users.getPassport());
                                    break;
                                case "UPDATE":
                                    userRootService.updateRecord(users);
                                    break;
                            }
                        }
                    }
                } catch (Exception e) {
                    log.error("Node " + port + ", -> " + e.getMessage());
                }
            }
        }
    }
}
