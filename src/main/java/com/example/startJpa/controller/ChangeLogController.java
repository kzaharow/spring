package com.example.startJpa.controller;

import com.example.startJpa.service.ChangeLogRootServiceImpl;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@RestController
@RequestMapping("changelog")
@RequiredArgsConstructor
@Slf4j
public class ChangeLogController {

    private final ChangeLogRootServiceImpl changeLogService;

    /**
     *
     * @return date of last record in the changelog table
     */
    @GetMapping(value = "lastDate", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getDateOfLastChangeLogRecord() {

        return ResponseEntity.ok(changeLogService.findDateOfLastChangeLogRecord());
    }

    /**
     *
     * @param queryDate the date (point) after that we want to get a list of changes
     * @return a list of changes from changelog tables
     */
    @GetMapping(value = "logs", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> getChangeLogChanges(@RequestParam(name = "date") String queryDate) {

        Date date = new Date();
        try {
            DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS");
            date = formatter.parse(queryDate);
        } catch (ParseException e) {
            log.error(e.getMessage());
        }
        return ResponseEntity.ok(changeLogService.findChangeLogRecordsForUpdate(date));
    }
}
