package com.example.startJpa.controller;


import com.example.startJpa.cluster.Cluster;
import com.example.startJpa.cluster.ClusterStatus;
import com.example.startJpa.node.Node;
import com.example.startJpa.node.NodeStatus;
import com.example.startJpa.cluster.ClusterRestManager;
import com.example.startJpa.dto.UserDto;
import com.example.startJpa.entity.Users;
import com.example.startJpa.service.UserRootServiceImpl;
import com.example.startJpa.utils.ChangeLogSaver;
import com.example.startJpa.utils.DtoToEntityConverter;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;

@RequestMapping("users")
@RestController
@RequiredArgsConstructor
@Slf4j
public class UserController {

    private final ChangeLogSaver changeLogSaver;

    private final UserRootServiceImpl userServiceImpl;

    private final Cluster cluster;

    private final Node node;

    private final ClusterRestManager clusterRestManager;

    private final DtoToEntityConverter dtoToEntityConverter;

    /**
     * @return all users from table
     */
    @GetMapping(value = "all", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findAll() {

        return ResponseEntity.ok().body(userServiceImpl.findAll());
    }

    /**
     * @param id specified passport id as HA0000001
     * @return a user whic was found , else return ResponseEntity.notFound() answer
     */
    @GetMapping(value = "passport", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> findByPassportNumber(@RequestParam(name = "id", defaultValue = "0") String id) {

        ResponseEntity<?> responseEntity = ResponseEntity.badRequest().build();
        // redirect to other cluster
        if (clusterRestManager.isRedirect(id)) {
            return clusterRestManager.redirectGet(responseEntity, id);
        }
        // current cluster handle
        if (cluster.getStatus() != ClusterStatus.RED) {
            if (node.getNodeStatus() == NodeStatus.SLAVE) {
                Users user = userServiceImpl.findUserByPassportId(id);
                if (user != null) {
                    responseEntity = ResponseEntity.ok().body(user);
                } else {
                    responseEntity = ResponseEntity.notFound().build();
                }
            } else {
                ArrayList<String> ports = new ArrayList<>(cluster.getPorts());
                for (String port : ports) {
                    try {
                        if (!port.equals(node.getPort())) {
                            responseEntity = new RestTemplate().getForEntity("http://localhost:" + port + "users/passport?id=" + id, String.class);
                            break;
                        }
                    } catch (Exception e) {
                        log.error(e.getMessage());
                    }
                }
            }
        }
        return responseEntity;
    }

    /**
     * @param userDto an object to store in database;
     * @return saved object with id from database;
     */
    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> saveUser(@RequestBody() UserDto userDto) {

        ResponseEntity<?> responseEntity = ResponseEntity.badRequest().build();
        // redirect to other cluster
        if (clusterRestManager.isRedirect(userDto.getPassport())) {
            return clusterRestManager.redirectPost(responseEntity, userDto);
        }
        // check if node is ready
        if (isReady()) {
            Users users = dtoToEntityConverter.convertToEntity(userDto);
            users = userServiceImpl.saveRecord(users);
            if (users != null) {
                changeLogSaver.saveChangeLog("POST", users);
                responseEntity = ResponseEntity.ok().body(users);
            }
        }
        return responseEntity;
    }

    /**
     * @param id passport id for delete
     * @return deleted user
     */
    @DeleteMapping(value = "passport", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> deleteUser(@RequestParam(name = "id", defaultValue = "1", required = false) String id) {

        ResponseEntity<?> responseEntity = ResponseEntity.notFound().build();
        // redirect to other cluster
        if (clusterRestManager.isRedirect(id)) {
            return clusterRestManager.redirectDelete(responseEntity, id);
        }
        // redirect to other cluster
        if (isReady()) {
            List<Users> users = userServiceImpl.deleteUserByPassportId(id);
            if (users.size() > 0) {
                changeLogSaver.saveChangeLog("DELETE", users.get(0));
                responseEntity = ResponseEntity.ok().body(users);
            }
        }
        return responseEntity;
    }

    /**
     * Update user info by passport id
     *
     * @param userDto - object with changes
     * @return void
     */
    @PutMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> updateUser(@RequestBody() UserDto userDto) {

        ResponseEntity<?> responseEntity = ResponseEntity.badRequest().build();
        if (isReady()) {
            Users users = dtoToEntityConverter.convertToEntity(userDto);
            userServiceImpl.updateRecord(users);
            changeLogSaver.saveChangeLog("UPDATE", users);
            return ResponseEntity.ok().build();
        }
        return responseEntity;
    }

    /**
     * Check if cluster / nodes is / are ready
     *
     * @return boolean
     */
    public boolean isReady() {
        return (cluster.getStatus() != ClusterStatus.RED)
                && (node.getNodeStatus() == NodeStatus.MASTER);
    }
}
