CREATE TABLE p10001.users
(
    id SERIAL NOT NULL,
    name TEXT,
    email VARCHAR(20),
    address_id INT PRIMARY KEY,
    CONSTRAINT users_address_id_fk FOREIGN KEY (address_id) REFERENCES p10001.address (id)
);
CREATE UNIQUE INDEX users_id_uindex ON p10001.users (id);

DO
$do$
    BEGIN
        FOR i IN 1..5 LOOP

                CREATE TABLE 'p10001.' || 't';
            END LOOP;
    END
$do$;