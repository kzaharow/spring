CREATE SCHEMA p20001;
CREATE SCHEMA p20002;
CREATE SCHEMA p20003;

/*SCHEMA1 --------------------------------------------------------------------------------------*/

CREATE TABLE p20001.changelog
(
    id   SERIAL PRIMARY KEY NOT NULL,
    type TEXT,
    data TEXT,
    date TIMESTAMP
);
CREATE UNIQUE INDEX changelog_id_uindex ON p20001.changelog (id);

CREATE TABLE p20001.cities
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX cities_id_uindex ON p20001.cities (id);

CREATE TABLE p20001.streets
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX streets_id_uindex ON p20001.streets (id);

CREATE TABLE p20001.address
(
    id          SERIAL PRIMARY KEY NOT NULL,
    city_id     INT,
    street_id   INT,
    home_number VARCHAR(5),
    CONSTRAINT address_cities_id_fk FOREIGN KEY (city_id) REFERENCES p20001.cities (id),
    CONSTRAINT address_streets_id_fk FOREIGN KEY (street_id) REFERENCES p20001.streets (id)
);

CREATE UNIQUE INDEX address_id_uindex ON p20001.address (id);

CREATE TABLE p20001.users
(
    id         SERIAL NOT NULL,
    passport   VARCHAR(9) UNIQUE,
    name       TEXT,
    email      VARCHAR(20),
    address_id INT PRIMARY KEY,
    CONSTRAINT users_address_id_fk FOREIGN KEY (address_id) REFERENCES p20001.address (id)
);
CREATE UNIQUE INDEX users_id_uindex ON p20001.users (id);

INSERT INTO p20001.changelog (type, data, date)
VALUES ('', '', '2000-01-01 00:00:01.000000');

/*SCHEMA2 --------------------------------------------------------------------------------------*/

CREATE TABLE p20002.changelog
(
    id   SERIAL PRIMARY KEY NOT NULL,
    type TEXT,
    data TEXT,
    date TIMESTAMP
);
CREATE UNIQUE INDEX changelog_id_uindex ON p20002.changelog (id);

CREATE TABLE p20002.cities
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX cities_id_uindex ON p20002.cities (id);

CREATE TABLE p20002.streets
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX streets_id_uindex ON p20002.streets (id);

CREATE TABLE p20002.address
(
    id          SERIAL PRIMARY KEY NOT NULL,
    city_id     INT,
    street_id   INT,
    home_number VARCHAR(5)
);

CREATE UNIQUE INDEX address_id_uindex ON p20002.address (id);

CREATE TABLE p20002.users
(
    id         SERIAL NOT NULL,
    passport   VARCHAR(9) UNIQUE,
    name       TEXT,
    email      VARCHAR(20),
    address_id INT PRIMARY KEY,
    CONSTRAINT users_address_id_fk FOREIGN KEY (address_id) REFERENCES p20002.address (id)
);
CREATE UNIQUE INDEX users_id_uindex ON p20002.users (id);

INSERT INTO p20002.changelog (type, data, date)
VALUES ('', '', '2000-01-01 00:00:01.000000');

/*SCHEMA3 --------------------------------------------------------------------------------------*/

CREATE TABLE p20003.changelog
(
    id   SERIAL PRIMARY KEY NOT NULL,
    type TEXT,
    data TEXT,
    date TIMESTAMP
);
CREATE UNIQUE INDEX changelog_id_uindex ON p20003.changelog (id);

CREATE TABLE p20003.cities
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX cities_id_uindex ON p20003.cities (id);

CREATE TABLE p20003.streets
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX streets_id_uindex ON p20003.streets (id);

CREATE TABLE p20003.address
(
    id          SERIAL PRIMARY KEY NOT NULL,
    city_id     INT,
    street_id   INT,
    home_number VARCHAR(5)
);
CREATE UNIQUE INDEX address_id_uindex ON p20003.address (id);

CREATE TABLE p20003.users
(
    id         SERIAL NOT NULL,
    passport   VARCHAR(9) UNIQUE,
    name       TEXT,
    email      VARCHAR(20),
    address_id INT PRIMARY KEY,
    CONSTRAINT users_address_id_fk FOREIGN KEY (address_id) REFERENCES p20003.address (id)
);
CREATE UNIQUE INDEX users_id_uindex ON p20003.users (id);

INSERT INTO p20003.changelog (type, data, date)
VALUES ('', '', '2000-01-01 00:00:01.000000');