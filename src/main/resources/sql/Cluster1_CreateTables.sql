CREATE SCHEMA p10001;
CREATE SCHEMA p10002;
CREATE SCHEMA p10003;

/*SCHEMA1 --------------------------------------------------------------------------------------*/

CREATE TABLE p10001.changelog
(
    id   SERIAL PRIMARY KEY NOT NULL,
    type TEXT,
    data TEXT,
    date TIMESTAMP
);
CREATE UNIQUE INDEX changelog_id_uindex ON p10001.changelog (id);

CREATE TABLE p10001.cities
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX cities_id_uindex ON p10001.cities (id);

CREATE TABLE p10001.streets
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX streets_id_uindex ON p10001.streets (id);

CREATE TABLE p10001.address
(
    id          SERIAL PRIMARY KEY NOT NULL,
    city_id     INT,
    street_id   INT,
    home_number VARCHAR(5),
    CONSTRAINT address_cities_id_fk FOREIGN KEY (city_id) REFERENCES p10001.cities (id),
    CONSTRAINT address_streets_id_fk FOREIGN KEY (street_id) REFERENCES p10001.streets (id)
);

CREATE UNIQUE INDEX address_id_uindex ON p10001.address (id);

CREATE TABLE p10001.users
(
    id         SERIAL NOT NULL,
    passport   VARCHAR(9) UNIQUE,
    name       TEXT,
    email      VARCHAR(20),
    address_id INT PRIMARY KEY,
    CONSTRAINT users_address_id_fk FOREIGN KEY (address_id) REFERENCES p10001.address (id)
);
CREATE UNIQUE INDEX users_id_uindex ON p10001.users (id);

INSERT INTO p10001.changelog (type, data, date)
VALUES ('', '', '2000-01-01 00:00:01.000000');

/*SCHEMA2 --------------------------------------------------------------------------------------*/

CREATE TABLE p10002.changelog
(
    id   SERIAL PRIMARY KEY NOT NULL,
    type TEXT,
    data TEXT,
    date TIMESTAMP
);
CREATE UNIQUE INDEX changelog_id_uindex ON p10002.changelog (id);

CREATE TABLE p10002.cities
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX cities_id_uindex ON p10002.cities (id);

CREATE TABLE p10002.streets
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX streets_id_uindex ON p10002.streets (id);

CREATE TABLE p10002.address
(
    id          SERIAL PRIMARY KEY NOT NULL,
    city_id     INT,
    street_id   INT,
    home_number VARCHAR(5)
);

CREATE UNIQUE INDEX address_id_uindex ON p10002.address (id);

CREATE TABLE p10002.users
(
    id         SERIAL NOT NULL,
    passport   VARCHAR(9) UNIQUE,
    name       TEXT,
    email      VARCHAR(20),
    address_id INT PRIMARY KEY,
    CONSTRAINT users_address_id_fk FOREIGN KEY (address_id) REFERENCES p10002.address (id)
);
CREATE UNIQUE INDEX users_id_uindex ON p10002.users (id);

INSERT INTO p10002.changelog (type, data, date)
VALUES ('', '', '2000-01-01 00:00:01.000000');

/*SCHEMA3 --------------------------------------------------------------------------------------*/

CREATE TABLE p10003.changelog
(
    id   SERIAL PRIMARY KEY NOT NULL,
    type TEXT,
    data TEXT,
    date TIMESTAMP
);
CREATE UNIQUE INDEX changelog_id_uindex ON p10003.changelog (id);

CREATE TABLE p10003.cities
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX cities_id_uindex ON p10003.cities (id);

CREATE TABLE p10003.streets
(
    id   SERIAL PRIMARY KEY NOT NULL,
    name TEXT UNIQUE
);
CREATE UNIQUE INDEX streets_id_uindex ON p10003.streets (id);

CREATE TABLE p10003.address
(
    id          SERIAL PRIMARY KEY NOT NULL,
    city_id     INT,
    street_id   INT,
    home_number VARCHAR(5)
);
CREATE UNIQUE INDEX address_id_uindex ON p10003.address (id);

CREATE TABLE p10003.users
(
    id         SERIAL NOT NULL,
    passport   VARCHAR(9) UNIQUE,
    name       TEXT,
    email      VARCHAR(20),
    address_id INT PRIMARY KEY,
    CONSTRAINT users_address_id_fk FOREIGN KEY (address_id) REFERENCES p10003.address (id)
);
CREATE UNIQUE INDEX users_id_uindex ON p10003.users (id);

INSERT INTO p10003.changelog (type, data, date)
VALUES ('', '', '2000-01-01 00:00:01.000000');